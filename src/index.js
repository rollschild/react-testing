/*
 * Root/Bootup location of the entire app
 */
import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route} from 'react-router-dom';
import Root from 'Root';
/*
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import reducers from 'reducers'; // default from index.js
*/
import App from 'components/App';

// An instance of the component
// ...and a reference to a DOM element on the page
/*
ReactDOM.render(
  <Root>
    <App />
  </Root>,
  document.querySelector('#root'),
);
*/

ReactDOM.render(
  <Root>
    <BrowserRouter>
      <Route path="/" component={App} />
    </BrowserRouter>
  </Root>,
  document.querySelector('#root'),
);
