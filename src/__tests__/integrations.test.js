import React from 'react';
import {mount} from 'enzyme';
import moxios from 'moxios';
import Root from 'Root';
import App from 'components/App';

beforeEach(() => {
  // setup moxios
  // intercept any request that axios tries to issue
  moxios.install();
  moxios.stubRequest('https://jsonplaceholder.typicode.com/comments', {
    status: 200,
    response: [{name: 'Fetched #1'}, {name: 'Fetched #2'}],
  });
});

afterEach(() => {
  moxios.uninstall();
});

it('can fetch a list of comments and display all of them', done => {
  // Render the *entire* app
  const wrapped = mount(
    <Root>
      <App />
    </Root>,
  );
  // find the *Fetch Comments* button and click it
  wrapped.find('#fetch').simulate('click');

  // a pause
  // ...and tell Jest to hold on
  /*
  setTimeout(() => {
    wrapped.update();
    // Expect to find a list of comments displayed
    expect(wrapped.find('li').length).toEqual(2);

    done();
    wrapped.unmount();
  }, 100);
  */

  moxios.wait(() => {
    wrapped.update();

    expect(wrapped.find('li').length).toEqual(2);

    done();
    wrapped.unmount();
  });
});
