/*
export default function({dispatch}) {
  return function(next) {
    return function(action) {};
  };
}
*/

export default ({dispatch}) => next => action => {
  /*
   * Check to see if the action
   * has a promise on its *payload* property.
   * If it does, then wait for it to resolve,
   * if does NOT, then send action on to the
   * next middleware.
   */

  if (!action.payload || !action.payload.then) {
    return next(action);
  }

  /*
   * Now we want to wait for the promise to resolve
   * (get its data!!!!!!!!) and then create a new action
   * with the data retrieved and dispatch it.
   */
  action.payload.then(function(response) {
    const newAction = {...action, payload: response};
    dispatch(newAction);
  });
};
