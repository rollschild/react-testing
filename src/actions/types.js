export const SAVE_COMMENT = 'save_comment';
export const FETCH_COMMENTS = 'fetch_comments';
export const TOGGLE_AUTH = 'toggle_auth';
