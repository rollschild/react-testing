import {SAVE_COMMENT} from 'actions/types';
import {saveComment} from 'actions';

describe('saveComment action', () => {
  it('has the correct action type', () => {
    const action = saveComment();
    expect(action.type).toEqual(SAVE_COMMENT);
  });

  it('has the correct action payload', () => {
    const action = saveComment('A comment...');
    expect(action.payload).toEqual('A comment...');
  });
});
