// action creaters
import axios from 'axios';
import {SAVE_COMMENT, FETCH_COMMENTS, TOGGLE_AUTH} from 'actions/types';

export function saveComment(comment) {
  return {
    type: SAVE_COMMENT,
    payload: comment,
  };
}

export function fetchComments() {
  const response = axios.get('https://jsonplaceholder.typicode.com/comments');

  return {
    type: FETCH_COMMENTS,
    payload: response,
  };
}

export function toggleAuth(isLoggedIn) {
  return {
    type: TOGGLE_AUTH,
    payload: isLoggedIn,
  };
}
