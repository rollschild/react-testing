import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actions from 'actions';
import requireAuth from 'components/requireAuth';

// This is a functional component
/*
export default () => {
  return <div>Comment Box</div>;
};
*/

// Class-based

class CommentBox extends Component {
  state = {comment: ''};

  handleChange = event => {
    // force the component to re-render
    // ...*asynchronously*
    this.setState({comment: event.target.value});
  };

  handleSubmit = event => {
    event.preventDefault(); // prevent page from reloading

    this.props.saveComment(this.state.comment);

    this.setState({comment: ''});
  };

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <h3>Enter a Comment</h3>
          <textarea onChange={this.handleChange} value={this.state.comment} />
          <div>
            <button>Submit Comment</button>
          </div>
        </form>
        <button id="fetch" onClick={this.props.fetchComments}>
          Fetch Comments
        </button>
      </div>
    );
  }
}

// action creators are passed as props to CommentBox
export default connect(
  null,
  actions,
)(requireAuth(CommentBox));
