import React, {Component} from 'react';
import {connect} from 'react-redux';

/*
export default () => {
  return <div>Comments List</div>;
};
*/

class CommentsList extends Component {
  renderComments() {
    return this.props.comments.map(comment => {
      return <li key={comment}>{comment}</li>;
    });
  }

  render() {
    return (
      <div>
        <h4>List of Comments</h4>
        <ul>{this.renderComments()}</ul>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {comments: state.comments};
}

export default connect(mapStateToProps)(CommentsList);
