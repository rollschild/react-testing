// Higher order component
// HOC scaffold

import React, {Component} from 'react';
import {connect} from 'react-redux';

export default ChildComponent => {
  class ComposedComponent extends Component {
    // Life cycle methods
    // this component just got rendered
    componentDidMount() {
      this.shouldNavigateAway();
    }

    // this component just got updated
    componentDidUpdate() {
      this.shouldNavigateAway();
    }

    shouldNavigateAway() {
      if (!this.props.auth) {
        // console.log('Redirecting...');
        this.props.history.push('/');
        // navigate around the application
      }
    }

    // Pass props down to child component
    // IMPORTANT STEP
    render() {
      return <ChildComponent {...this.props} />;
    }
  }

  function mapStateToProps(state) {
    return {auth: state.auth};
  }

  return connect(mapStateToProps)(ComposedComponent);
};
