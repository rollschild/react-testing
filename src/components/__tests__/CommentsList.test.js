import React from 'react';
import {mount} from 'enzyme';
import CommentsList from 'components/CommentsList';
import Root from 'Root';

let wrapped;

beforeEach(() => {
  const initialState = {
    comments: ['Comment One', 'Comment 2'],
  };

  wrapped = mount(
    <Root initialState={initialState}>
      <CommentsList />
    </Root>,
  );
});

it('creates one list item per comment', () => {
  // console.log(wrapped.find('li').length);
  expect(wrapped.find('li').length).toEqual(2);
});

it('shows correct text for each comment', () => {
  // CheerioWrapper
  expect(wrapped.render().text()).toContain('Comment One');
  expect(wrapped.render().text()).toContain('Comment 2');
});
