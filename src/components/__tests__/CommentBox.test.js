import React from 'react';
import {mount} from 'enzyme';
import CommentBox from 'components/CommentBox';
import Root from 'Root';

let wrapped;

beforeEach(() => {
  // Full DOM
  wrapped = mount(
    <Root>
      <CommentBox />
    </Root>,
  );
});

afterEach(() => {
  // cleanup full DOM mount
  wrapped.unmount();
});

it('has a text input area, a submit button, and a fetch button', () => {
  expect(wrapped.find('textarea').length).toEqual(1);
  expect(wrapped.find('form button').length).toEqual(1);
  expect(wrapped.find('#fetch').length).toEqual(1);
});

// Group a set of tests
describe('the text input area', () => {
  beforeEach(() => {
    wrapped.find('textarea').simulate('change', {
      target: {value: 'A new test comment...'},
    });

    // need to force the component to re-render
    wrapped.update();
  });

  it('has a text input area that can show user inputs', () => {
    expect(wrapped.find('textarea').prop('value')).toEqual(
      'A new test comment...',
    );
  });

  it('clears the input box when submit is clicked', () => {
    expect(wrapped.find('textarea').prop('value')).toEqual(
      'A new test comment...',
    );

    wrapped.find('form').simulate('submit');
    wrapped.update();

    expect(wrapped.find('textarea').prop('value')).toEqual('');
  });
});
