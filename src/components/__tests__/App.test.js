import React from 'react';
import {shallow} from 'enzyme';
// import ReactDOM from 'react-dom';
import App from 'components/App';
import CommentBox from 'components/CommentBox';
import CommentsList from 'components/CommentsList';

let wrapped;
// Notice the scope of `wrapped`
beforeEach(() => {
  wrapped = shallow(<App />);
});

it('shows a comment box', () => {
  // --------NOT IDEAL---------------------------------
  // const div = document.createElement('div');
  // the `div` element exist only in memory, inside terminal,
  // ...created by JSDOM,
  // ...and does NOT exist in any browser.
  // ReactDOM.render(<App />, div);
  // console.log(div.innerHTML);
  // NOT ideal:
  // expect(div.innerHTML).toContain('Comment Box');
  // remove the App component
  // cleanup
  // ReactDOM.unmountComponentAtNode(div);
  // ---------------------------------------------------

  // const wrapped = shallow(<App />);
  expect(wrapped.find(CommentBox).length).toEqual(1);
});

it('shows a list for comments', () => {
  // const wrapped = shallow(<App />);
  expect(wrapped.find(CommentsList).length).toEqual(1);
});
