import React, {Component} from 'react';
import {Route, Link} from 'react-router-dom';
import {connect} from 'react-redux';
import CommentBox from 'components/CommentBox';
import CommentsList from 'components/CommentsList';
import * as actions from 'actions';

/*
export default () => {
  return (
    <div>
      <CommentBox />
      <CommentsList />
    </div>
  );
};
*/

class App extends Component {
  renderButton() {
    if (this.props.auth) {
      return (
        <button onClick={() => this.props.toggleAuth(false)}>Sign Out</button>
      );
    } else {
      return (
        <button onClick={() => this.props.toggleAuth(true)}>Sing In</button>
      );
    }
  }

  renderHeader() {
    return (
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/post">Post a Comment</Link>
        </li>
        <li>{this.renderButton()}</li>
      </ul>
    );
  }

  render() {
    return (
      <div>
        {this.renderHeader()}
        <Route path="/post" component={CommentBox} />
        <Route path="/" exact component={CommentsList} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {auth: state.auth};
}

export default connect(
  mapStateToProps,
  actions,
)(App);
